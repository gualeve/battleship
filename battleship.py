#!/usr/bin/python3
'''
Battleship - Jogo Batalha Naval.
Author - José Adalberto F. Gualeve
Create - 10/20/2018
Version 1.0
'''


class Jogo(object):
    """docstring for Jogo"""
    def __init__(self, partida):
        super(Jogo, self).__init__()
        self.partida = partida
        self.jogador = []

    def limpar_tela(self, linhas=100):
        print('\n' * linhas)

    def instrucoes(self, inicio, tamanho):
        self.limpar_tela()
        try:
            with open('battleship_pt_br.txt') as instrucoes:
                for linha in instrucoes.readlines():
                    print(linha)
        except FileNotFoundError:
            print('Arquivo de inicialização do jogo não encontrado')
            raise

    def fogos(self, vencedor):
        self.limpar_tela()
        print(f'Parabéns {vencedor.nome.upper()}, você venceu a batalha')
        self.limpar_tela(10)

    def novo_jogador(self, jogador):
        self.jogador.append(jogador)

    def turn(self, atacante, alvejado):
        disparo = False
        while not disparo:
            disparo = input('Digite uma coordenada: ')
            coordenada = atacante.tabuleiro.coordenada_xy(disparo)
            lin = coordenada[0]
            col = coordenada[1]
            if coordenada is not None:
                if alvejado.tabuleiro.area[lin][col]['aberto'] != '.':
                    disparo = False
        acerto = alvejado.tabuleiro.area[lin][col]['tipo']  # casa oculta
        if acerto == '.':
            acerto = 'x'
            print('Tchibummm!!!!')
            proximo = alvejado
        else:
            # acertou uma embarcacao.
            embarcacao = alvejado.tabuleiro.area[lin][col]['tipo']
            atingido = alvejado.tabuleiro.area[lin][col]['id']
            intacto = alvejado.tabuleiro.area[lin][col]['intacto'] - 1
            for i in range(10):
                if alvejado.tabuleiro.area[i][col]['id'] == atingido:
                    alvejado.tabuleiro.area[i][col]['intacto'] = intacto
                if alvejado.tabuleiro.area[lin][i]['id'] == atingido:
                    alvejado.tabuleiro.area[lin][i]['intacto'] = intacto
            # verificar se a embarcacao foi completamente destruida ou nao
            if alvejado.tabuleiro.area[lin][col]['intacto'] == 0:
                destruido = alvejado.tabuleiro.area[lin][col]['id']
                #  percorrer o tabuleiro para mostrar a embarcacao destruida
                for i in range(10):
                    if alvejado.tabuleiro.area[i][col]['id'] == destruido:
                        alvejado.tabuleiro.area[i][col]['aberto'] = acerto
                    if alvejado.tabuleiro.area[lin][i]['id'] == destruido:
                        alvejado.tabuleiro.area[lin][i]['aberto'] = acerto
            else:
                acerto = 'H'
            alvejado.tabuleiro.acertos_necessarios -= 1
            print('Chash!!!')
            proximo = atacante
        alvejado.tabuleiro.area[lin][col]['aberto'] = acerto
        if alvejado.tabuleiro.acertos_necessarios == 0:
            return (atacante, proximo)
        else:
            return (None, proximo)


class Embarcacao():
    def __init__(self):
        self.coord1 = None
        self.coord2 = None
        self.intacto = intacto

    def set_coordenadas(self, coord1, coord2):
        self.coord1 = coord1
        self.coord2 = coord2


class PortaAviao(Embarcacao):
    def __init__(self, id):
        self.tamanho = 5
        self.marca = 'P'
        self.intacto = self.tamanho
        self.id = id

    def __str__(self):
        return "Porta Avião"


class NavioTanque(Embarcacao):
    def __init__(self, id):
        self.tamanho = 4
        self.marca = 'T'
        self.intacto = 4
        self.id = id

    def __str__(self):
        return "Navio Tanque"


class Contratorpedeiro(Embarcacao):
    def __init__(self, id):
        self.tamanho = 3
        self.marca = 'C'
        self.intacto = 3
        self.id = id

    def __str__(self):
        return "Contratorpedeiro"


class Submarino(Embarcacao):
    def __init__(self, id):
        self.tamanho = 2
        self.marca = 'S'
        self.intacto = 2
        self.id = id

    def __str__(self):
        return "Submarino"


class Tabuleiro():
    def __init__(self, jogador):
        self.area = [[
                    {'tipo': '.',
                     'aberto': '.',
                     'id': None,
                     'intacto': 0} for x in range(10)] for y in range(10)]
        self.jogador = jogador
        self.acertos_necessarios = 0


    def mostrar(self, fase):
        letras = iter('ABCDEFGHIJ')
        if fase == 0:
            key = 'tipo'
        elif fase == 1:
            key = 'aberto'
        elif fase == 'debug':
            key = 'intacto'
        print('  0  1  2  3  4  5  6  7  8  9')
        for area in self.area:
            print(next(letras), end=' ')
            for casa in area:
                print(casa[key], end='  ')
            print('')
        return

    def coordenada_xy(self, coord_ax):
        letras = {'A': 0,
                  'B': 1,
                  'C': 2,
                  'D': 3,
                  'E': 4,
                  'F': 5,
                  'G': 6,
                  'H': 7,
                  'I': 8,
                  'J': 9}
        xy = iter(coord_ax)
        temp = next(xy).upper()
        if len(coord_ax) == 2 and temp in letras:
            lin = letras[temp]
            col = int(next(xy))
            return (lin, col)  # (coluna, linha)
        else:
            return None  # coordenada inválida

    def coordenada_ax(self, coord_xy):
        numeros = {0: 'A',
                   1: 'B',
                   2: 'C',
                   3: 'D',
                   4: 'E',
                   5: 'F',
                   6: 'G',
                   7: 'H',
                   8: 'I',
                   9: 'J'}
        #  coord_xy  -> (x, y)
        return str(numeros[coord_xy[1]])+str(coord_xy[0])

    def posicionar_embarcacao(self, coord1, coord2, embarcacao):

        lin1 = coord1[0]
        col1 = coord1[1]
        lin2 = coord2[0]
        col2 = coord2[1]
        vertical = col1 == col2
        horizontal = lin1 == lin2
        if vertical and lin2 - lin1 + 1 == embarcacao.tamanho:
            # verificar possível choque de embarcações:
            choque = False
            for i in range(embarcacao.tamanho):
                if self.area[lin1+i][col1]['tipo'] != '.':
                    choque = True
                    break
            if choque:
                retorno = False
            else:
                # preencher a embarcação no tabuleiro (vertical)
                for i in range(embarcacao.tamanho):
                    self.area[lin1+i][col1]['tipo'] = embarcacao.marca
                    self.area[lin1+i][col1]['id'] = embarcacao.id
                    self.area[lin1+i][col1]['intacto'] = embarcacao.tamanho
                retorno = True
        elif horizontal and col2 - col1 + 1 == embarcacao.tamanho:
            # verificar possível choque de embarcações:
            choque = False
            for i in range(embarcacao.tamanho):
                if self.area[lin1][col1+i]['tipo'] != '.':
                    choque = True
                    break
            if choque:
                retorno = False
            else:
                # preencher a embarcação no tabuleiro (horizontal)
                for i in range(embarcacao.tamanho):
                    self.area[lin1][col1+i]['tipo'] = embarcacao.marca
                    self.area[lin1][col1+i]['id'] = embarcacao.id
                    self.area[lin1][col1+i]['intacto'] = embarcacao.tamanho
                retorno = True
        else:
            print(f'Dimensao para {embarcacao} errada')
            retorno = False
        if retorno:
            self.acertos_necessarios += embarcacao.tamanho
        return retorno


class Jogador():
    def __init__(self, nome):
        self.nome = nome
        self.tabuleiro = Tabuleiro(nome)
        #self.tabuleiro_aberto = Tabuleiro(nome)
        self.porta_aviao = PortaAviao(1)
        self.naviotanque = [NavioTanque(2),
                            NavioTanque(3)]
        self.contratorpedeiro = [
                                Contratorpedeiro(4),
                                Contratorpedeiro(5),
                                Contratorpedeiro(6)]
        self.submarino = [
                        Submarino(7),
                        Submarino(8),
                        Submarino(9),
                        Submarino(10)]

    def posicionar(self):
        armada = [
                    self.porta_aviao,
                    self.naviotanque[0],
                    self.naviotanque[1],
                    self.contratorpedeiro[0],
                    self.contratorpedeiro[1],
                    self.contratorpedeiro[2],
                    self.submarino[0],
                    self.submarino[1],
                    self.submarino[2],
                    self.submarino[3]]
        formacao = ['']
        for embarcacao in armada:
            jogo.limpar_tela()
            for nave in formacao:
                if nave != '':
                    print(nave, ' (OK)')
            formacao.append(embarcacao)
            print('\n')
            ok = False
            while not ok:
                (coord1, coord2) = self.entrar_coordenadas(embarcacao)
                print(coord1, coord2)
                # (coordenadas no formato (lin, col), (lin, col))
                embarcacao.set_coordenadas(coord1, coord2)
                ok = self.tabuleiro.posicionar_embarcacao(coord1,
                                                          coord2,
                                                          embarcacao)
        return

    def entrar_coordenadas(self, unidade):
        coord1 = coord2 = 'invalida'
        self.tabuleiro.mostrar(fase=0)
        lin = col = 10
        # validar posicoes das embarcacoes
        while lin + unidade.tamanho > 9 and col + unidade.tamanho > 9:
            print(f'\nEmbarcacao: {unidade}')
            print('Primeira Coordenada: ')
            coord1 = self.tabuleiro.coordenada_xy(input('LinhaColuna <Ex.:G5>: '))
            #  coord1 deve ja estara no formato (x, y)
            if coord1 is None:
                print('Coordenada Inválida.')
                continue
            lin = coord1[0]  # número da linha
            col = coord1[1]  # número da coluna
            if lin + unidade.tamanho > 10 and col + unidade.tamanho > 10:
                # impossível por embarcação nesta coordenada
                print('Coordenada Inválida.')
            elif col + unidade.tamanho > 10:
                # impossível por embarcação nesta linha (forçar na Vertical)
                direcao = 'V'
                print(f'{unidade} colocada na vertical a partir de', end=' ')
                print(f'{self.tabuleiro.coordenada_ax(coord1)}')
            elif lin + unidade.tamanho > 10:
                # impossível embarcação nesta coluna (forçar na horizontal)
                direcao = 'H'
                print(f'{unidade} colocada na horizontal a partir de', end=' ')
                print(f'{self.tabuleiro.coordenada_ax(coord1)}')
            else:
                # a unidade cabe tanto na vertical quanto na horizontal.
                while True:
                    direcao = input('[V]ertical ou [H]orizontal? ').upper()
                    if direcao in 'VH':
                        break
        if direcao == 'V':
            coord2 = (lin + unidade.tamanho-1, col)
        else:
            coord2 = (lin, col + unidade.tamanho-1)

        print(f'Definido {unidade} em:', end=' ')
        print(f'<{self.tabuleiro.coordenada_ax(coord1)}-{self.tabuleiro.coordenada_ax(coord1)}>')

        return coord1, coord2   # (lin, col), (lin, col)


if __name__ == '__main__':
    jogo = Jogo('Partida Memorável')
    jogo.instrucoes(0, 9)
    nome_jogador = input('Jodador 1: ')
    jogador1 = Jogador(nome_jogador)
    jogador1.posicionar()
    jogo.novo_jogador(jogador1)
    jogo.limpar_tela()
    nome_jogador = input('Jodador 2: ')
    jogador2 = Jogador(nome_jogador)
    jogador2.posicionar()
    jogo.novo_jogador(jogador2)
    jogo.limpar_tela()
    vencedor = (None, jogador1)
    while vencedor[0] is None:
        print(f'\nTabuleiro do Jogador: {jogador1.nome}')
        jogador1.tabuleiro.mostrar(fase=1)
        #jogador1.tabuleiro.mostrar(fase='debug')
        print(f'\nTabuleiro do Jogador: {jogador2.nome}')
        jogador2.tabuleiro.mostrar(fase=1)
        #jogador2.tabuleiro.mostrar(fase='debug')
        if vencedor[1] == jogador1:
            print(f'\n{jogador1.nome}, disparar no tabuleiro de {jogador2.nome}')
            vencedor = jogo.turn(atacante=jogador1, alvejado=jogador2)
        else:
            print(f'\n{jogador2.nome}, disparar no tabuleiro de {jogador1.nome}')
            vencedor = jogo.turn(atacante=jogador2, alvejado=jogador1)
    jogo.fogos(vencedor[1])
